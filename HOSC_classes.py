# Author - Andrei Bobu
# This code provides a realization of higher-order spectral clustering for planted clique recovery
# in random geometric and Waxman graphs


import numpy as np
import networkx as nx
import random
import matplotlib.pyplot as plt
from scipy import sparse
import itertools
import time
import seaborn as sns


def distance(v_1, v_2):
    """
    This functions returns the torus distance between positions in [0, 1]^2
    :param v_1: numpy.array with position of node 1
    :param v_2: numpy.array with position of node 2
    :return: float
    """
    if len(v_1) != len(v_2):
        return -1
    else:
        n = len(v_1)

    diff = np.zeros(n)
    for i in range(n):
        diff[i] = min(abs(v_1[i] - v_2[i]), 1 - abs(v_1[i] - v_2[i]))

    return np.linalg.norm(diff)


def define_border(x, y, r):
    if x < r:
        x_border = "left"
    elif x > 1 - r:
        x_border = "right"
    else:
        x_border = "middle"
        
    if y < r:
        y_border = "bottom"
    elif y > 1 - r:
        y_border = "top"
    else:
        y_border = "middle"
    
    return (x_border, y_border)


class RggPlantedClique(nx.Graph):
    def __init__(self, n=1000, r=0.01, k=10, clique='random'):
        # Initialize the graph as a random geometric graph 
        super().__init__()
        RGG = nx.random_geometric_graph(n=n, radius=r)
        self.add_nodes_from(RGG)
        self.add_edges_from(RGG.edges)
        pos = nx.get_node_attributes(RGG, 'pos')
        nx.set_node_attributes(self, pos, 'pos')

        # RGG is implemented with Euclidean metric. We have to check only the nodes on the boundary of the unit square.
        #         boundary_nodes = [v for v in RGG if check_if_boundary(RGG.nodes[v]['pos'], r)]
#         for (u, v) in itertools.combinations(boundary_nodes, 2):
#             if distance(self.nodes[u]['pos'], self.nodes[v]['pos']) <= r:
#                 self.add_edge(u, v)
        left_nodes, right_nodes, bottom_nodes, top_nodes = [], [], [], []
        for v in RGG:
            (x, y) = (RGG.nodes[v]['pos'][0], RGG.nodes[v]['pos'][1])
            if x < r:
                left_nodes.append(v)
            elif x > 1 - r:
                right_nodes.append(v)

            if y < r:
                bottom_nodes.append(v)
            elif y > 1 - r:
                top_nodes.append(v)
        
        for u in left_nodes:
            for v in right_nodes:
                if distance(self.nodes[u]['pos'], self.nodes[v]['pos']) <= r:
                    self.add_edge(u, v)
                    
        for u in bottom_nodes:
            for v in top_nodes:
                if distance(self.nodes[u]['pos'], self.nodes[v]['pos']) <= r:
                    self.add_edge(u, v)

        # Set planted clique
        self.k = k
        self.r = r
        self.planted_clique = []

        if clique == 'random':
            # Simply pick k random nodes
            self.planted_clique = random.sample(self.nodes, k)
        elif clique == 'geometric':
            # This formula guarantees that we have on average k nodes in circle of radius R
            self.R = np.sqrt(k / (np.pi * n))

            # Define a random center (x_random, y_random) of the planted clique
            x_random = random.random()
            y_random = random.random()
            center = [x_random, y_random]

            # Add nodes that inside the circle with center (x_random, y_random) of radius R to the planted clique
            for v in self:
                if distance(center, self.nodes[v]['pos']) < self.R:
                    self.planted_clique += [v]

        # Add artificial edges
        for (i, j) in itertools.combinations(self.planted_clique, 2):
            self.add_edge(i, j)

    # Plot the graph with planted clique marked by red color
    def plot(self):
        fig, ax1 = plt.subplots(1, 1, share=True, figsize=(14, 7))
        pos = nx.spring_layout(self)
        ground_colors = ['red' if v in self.planted_clique else 'gray' for v in self.nodes]
        nx.draw(self, pos, ax1, with_labels=True, node_color=ground_colors)
        plt.show()
        plt.close()

    # Accuracy of the guess
    def get_accuracy(self, hat_planted_clique):
        right_guess = set(self.planted_clique) & set(hat_planted_clique)
        right_guess_cnt = len(right_guess)
        return float(right_guess_cnt / len(self.planted_clique))
    
    def list_nodes(self):
        for v in self:
            print(v, self.nodes[v]['pos'])
            
    def list_pairs(self):
        for (u, v) in itertools.combinations(self, 2):
            print(u, v, self.nodes[u]['pos'], self.nodes[v]['pos'], distance(self.nodes[u]['pos'], self.nodes[v]['pos']), distance(self.nodes[u]['pos'], self.nodes[v]['pos']) < self.r, self.has_edge(u, v))


class WaxmanPlantedClique(nx.Graph):
    def __init__(self, n=1000, alpha=0.25, beta=0.5, k=10):
        # Initialize the graph as an empty graph
        super().__init__()
        for i in range(n):
            pos = [random.random(), random.random()]
            self.add_node(i, pos=pos)

        p_array = []
        # Add the edges according to probability in Waxman graphs
        for (i, j) in itertools.combinations(self.nodes, 2):
            d = distance(self.nodes[i]['pos'], self.nodes[j]['pos'])
            p = beta * np.exp(-d / alpha)
            rand_val = random.random()
            if rand_val <= p and d <= 1 / 2:
                self.add_edge(i, j)

        # Set planted clique
        self.k = k
        self.alpha = alpha
        self.beta = beta
        self.planted_clique = random.sample(self.nodes, k)
        self.p_array = p_array

        # Add artificial edges
        for (i, j) in itertools.combinations(self.planted_clique, 2):
            self.add_edge(i, j)

    # Plot the graph with planted clique marked by red color
    def plot(self):
        fig, ax1 = plt.subplots(1, 1, figsize=(14, 7))
        pos = nx.spring_layout(self)
        ground_colors = ['red' if v in self.planted_clique else 'gray' for v in self.nodes]
        nx.draw(self, pos, ax1, with_labels=True, node_color=ground_colors)
        plt.show()
        plt.close()

    # Accuracy of the guess
    def get_accuracy(self, hat_planted_clique):
        right_guess = set(self.planted_clique) & set(hat_planted_clique)
        right_guess_cnt = len(right_guess)
        return float(right_guess_cnt / len(self.planted_clique))


# Class that provides HOSC on the RGG with the planted clique
class HOSC:

    def __init__(self, G, portion=0.05):

        n = G.number_of_nodes()
        k = G.k
        d = sum(dict(G.degree).values()) / n  # Average degree in G
        adjacency_matrix = nx.adjacency_matrix(G)

        # dense_matrix = adjacency_matrix.toarray()
        # q_matrix = np.pi * G.r * G.r * np.ones((n, n))
        # dense_matrix = dense_matrix - q_matrix
        # vals, vecs = np.linalg.eig(dense_matrix)

        vals, vecs = sparse.linalg.eigs(adjacency_matrix.asfptype(),
                                        k=int(portion * n),
                                        which='LM')
        # optimal_val = G.k * (1 - np.pi * G.r * G.r) / (1 + G.k / n)
        # optimal_val = G.k * (1 - 4 * np.pi * G.r * G.r)
        optimal_val = 1 / 2 * (k + d * (n - k) + np.sqrt(
            d * d * n * n + 2 * d * (d - 1) * k * n - k * k * (3 * d * d - 2 * d - 1)))
        # print(vals)
        accs = []
        min_l1_norm = 100000
        res_acc = 0

        best_eigenvalue_idx = closest(vals, optimal_val)
        vector = vecs[:, best_eigenvalue_idx]
        vector = vector.astype('float64')
        # print(vector)

        if abs(max(vector)) > abs(min(vector)):
            threshold = np.quantile(vector, float(1 - G.k / n))
            hat_planted_clique = np.argwhere(vector >= threshold).flatten()
        else:
            threshold = np.quantile(vector, float(G.k / n))
            hat_planted_clique = np.argwhere(vector <= threshold).flatten()

        acc = G.get_accuracy(hat_planted_clique)
        self.accuracy = acc

        # for i in range(len(vals)):
        #     vector = vecs[:, i]
        #     vector = vector.astype('float64')
        #     # print(vector)
        #
        #     if abs(max(vector)) > abs(min(vector)):
        #         threshold = np.quantile(vector, float(1 - G.k / n))
        #         hat_planted_clique = np.argwhere(vector >= threshold).flatten()
        #     else:
        #         threshold = np.quantile(vector, float(G.k / n))
        #         hat_planted_clique = np.argwhere(vector <= threshold).flatten()
        #
        #     acc = G.get_accuracy(hat_planted_clique)
        #     accs += [acc]
        #
        #     # if abs(vals[i] - optimal_val) < 5:
        #     #     sns.distplot(vecs[:, i], kde = False, bins = 50)
        #     #     plt.title("Vector " + str(i) + ", distance to optimal value = " + str(round(abs(vals[i] - optimal_val), 3)) + ", accuracy = " + str(round(max(acc1, acc2), 3)) + ", l_1 norm = " + str(round(np.linalg.norm(vecs[:, i], ord=1), 3)))
        #     #     plt.show()
        #
        #     if abs(vals[i] - optimal_val) < 0.1 * optimal_val:
        #         l_1_norm = np.linalg.norm(vecs[:, i], ord=1)
        #         if l_1_norm < min_l1_norm:
        #             res_acc = acc
        #             min_l1_norm = l_1_norm

        # sns.set()
        # plt.scatter(range(len(vals)), accs, marker='o')
        #
        # plt.xlabel("Order of eigenvector")
        # plt.ylabel("Accuracy")
        # plt.show()
        # plt.close()
        #
        # sns.set()
        # plt.scatter(vals, accs, marker='o')
        # plt.xlabel("Eigenvalue")
        # plt.ylabel("Accuracy")
        # plt.axvline(x=optimal_val, color='black')
        # plt.show()
        # plt.close()

        # self.accuracy = res_acc


def closest(array, val):
    diff_array = [abs(x - val) for x in array]
    return np.argmin(diff_array)


class HOSC_algorithm_for_ER:

    def __init__(self, G, portion=0.001):

        n = G.number_of_nodes()
        adjacency_matrix = nx.adjacency_matrix(G)

        # dense_matrix = adjacency_matrix.toarray()
        # q_matrix = np.pi * G.r * G.r * np.ones((n, n))
        # dense_matrix = dense_matrix - q_matrix
        # vals, vecs = np.linalg.eig(dense_matrix)

        vals, vecs = sparse.linalg.eigs(adjacency_matrix.asfptype(),
                                        k=int(portion * n),
                                        which='LM')
        vector = vecs[:, 1]
        vector = vector.astype('float64')
        # print(vector)

        if abs(max(vector)) > abs(min(vector)):
            threshold = np.quantile(vector, float(1 - G.k / n))
            hat_planted_clique = np.argwhere(vector >= threshold).flatten()
        else:
            threshold = np.quantile(vector, float(G.k / n))
            hat_planted_clique = np.argwhere(vector <= threshold).flatten()

        self.accuracy = G.get_accuracy(hat_planted_clique)

        # sns.set()
        # plt.scatter(range(len(vals)), accs, marker='o')

        # plt.xlabel("Order of eigenvector")
        # plt.ylabel("Accuracy")
        # plt.show()
        # plt.close()

        # sns.set()
        # plt.scatter(vals, accs, marker='o')
        # plt.xlabel("Eigenvalue")
        # plt.ylabel("Accuracy")
        # plt.axvline(x = optimal_val, color = 'black')
        # plt.show()
        # plt.close()


class VD:
    def __init__(self, G):
        # Sort the degrees of the graph
        degrees = sorted(G.degree, key=lambda x: x[1], reverse=True)

        # Make a list of nodes with k highest degrees - this is our prediction
        nodes_sorted_by_degree = [x[0] for x in degrees]
        hat_planted_clique = nodes_sorted_by_degree[:G.k]

        # Calculate accuracy of such a prediction
        acc = G.get_accuracy(hat_planted_clique)
        self.accuracy = acc


class CN:
    def __init__(self, G):
        # Make a list of neighbours for each node
        list_neighbrs = {}
        for v in G.nodes:
            list_neighbrs.update({v: set(G.neighbors(v))})

        hat_planted_clique = set()  # Default value for the prediction
        stop_flg = False  # stop_flg = 1 means that we have found an appropriate clique

        # Iterate over pairs of adjacent nodes
        for u in G.nodes:
            if not stop_flg:
                for v in list_neighbrs[u]:
                    if not stop_flg and u < v:  # Don't need to check the same pair twice
                        # Count the number of common neighbours
                        cmn_nghbr_cnt = len(list_neighbrs[u] & list_neighbrs[v])
                        if cmn_nghbr_cnt == G.k - 2:
                            # If the number of common neighbours of a pair is exactly k - 2, form a k-set of them
                            potential_clique = (list_neighbrs[u] & list_neighbrs[v]) | {u, v}

                            # Check if this potential clique is really a clique
                            if is_subclique(G, potential_clique):
                                hat_planted_clique = potential_clique
                                stop_flg = 1
                                self.u = u
                                self.v = v

        # Calculate accuracy
        acc = G.get_accuracy(hat_planted_clique)
        self.accuracy = acc
        self.hat_planted_clique = hat_planted_clique
        self.list_neighbrs = list_neighbrs


def is_subclique(G, nodelist):
    """
    This function checks if nodelist forms a clique in graph G
    :param G: nx.Graph -- a graph implemented in networkx
    :param nodelist: array_like -- a list of nodes of graph G
    :return: bool -- True if nodelist forms a clique in graph G, False otherwise
    """
    edges_num = 0
    for (i, j) in itertools.combinations(nodelist, 2):
        if G.has_edge(i, j):
            edges_num += 1
    n = len(nodelist)
    return edges_num == n * (n - 1) / 2


class CC:
    def __init__(self, G):
        # Sort the closeness centralities of a graph
        centralities = sorted(nx.closeness_centrality(G).items(), key=lambda x: x[1], reverse=True)

        # Make a list of nodes with k largest closeness centralities - this is our prediction
        nodes_sorted_by_cc = [x[0] for x in centralities]
        hat_planted_clique = nodes_sorted_by_cc[:G.k]

        # Calculate accuracy of such a prediction
        acc = G.get_accuracy(hat_planted_clique)
        self.accuracy = acc


class BC:
    def __init__(self, G):
        # Sort the betweenness centralities of a graph
        centralities = sorted(nx.betweenness_centrality(G).items(), key=lambda x: x[1], reverse=True)

        # Make a list of nodes with k largest betweenness centralities - this is our prediction
        nodes_sorted_by_cc = [x[0] for x in centralities]
        hat_planted_clique = nodes_sorted_by_cc[:G.k]

        # Calculate accuracy of such a prediction
        acc = G.get_accuracy(hat_planted_clique)
        self.accuracy = acc
        

def full_simulation(algo_list,
                    n,
                    r,
                    k_start=1,
                    k_finish=2,
                    k_step=1,
                    n_trials=10,
                    output_file_path=None,
                    inline_plot=False,
                    plots_dir=None):
    """
    This function runs full simulation.
    For each k from interval [k_start, k_finish], it generates n_trials RGG(n, r) with a planted clique of size k.
    Then it runs each algorithm from algo_list, prints and returns obtained average accuracies.
    :param algo_list: array_like -- list of algo names to check during the simulation. Algorithms have to be classes.
    :param n: int -- Number of nodes in RGG
    :param r: double -- Radius of interaction in RGG
    :param k_start: double -- Beginning of the interval [k_start, k_finish]
    :param k_finish: double -- Finish of the interval [k_start, k_finish]
    :param k_step: double -- Magnitude of a step when we go through the interval [k_start, k_finish]
    :param n_trials: int -- Number of generated graphs for each k
    :param output_file_path: str -- Path to the file where to save the results, optional
    :param inline_plot: bool -- Whether to show the plot inline
    :param plots_dir: str -- Path to folder where to save the plots
    :return: dict
    """
    # Create an array with possible values of accuracies
    acc_array = []

    # Clear the output file if it is set
    if output_file_path is not None:
        open(output_file_path, 'w').close()

    for k in np.arange(k_start, k_finish, k_step):
        cur_step = {"k": k}
        # Form the beginning of a string to print
        s = f"k = {k}, r = {r}, n = {n}, time for RGG generating = "

        # Form a dictionary with accuracies and times of execution for this k
        acc_dict = {A.__name__: 0 for A in algo_list}
        time_dict = {A.__name__: 0 for A in algo_list}
        time_dict.update({"RGG generating": 0})

        # Generate graphs and check accuracy
        for i in range(n_trials):
            start_time = time.time()
            G = RggPlantedClique(n, r, k, clique='random')
            time_sec = time.time() - start_time
            time_dict["RGG generating"] += time_sec

            # In a given trial, we use the same graph for all the algorithms
            for A in algo_list:
                # Launch algo A on graph G
                start_time = time.time()
                alg_res = A(G)
                time_sec = time.time() - start_time

                # Get accuracy and norm it by the number of trials in order to obtain average accuracy
                acc_dict[A.__name__] += alg_res.accuracy / n_trials
                time_dict[A.__name__] += time_sec

        # Form the string with results to print
        s = s + f"{int(time_dict['RGG generating'])} sec, "
        for A in algo_list:
            s = s + f"{A.__name__} = {np.around(acc_dict[A.__name__] * 100)}% ({int(time_dict[A.__name__])} sec), "
        print(s)

        # Save the same string to the file if needed
        if output_file_path is not None:
            with open(output_file_path, "a") as f:
                f.write(f"{s}\n")
                f.close()

        # Add all the accuracies to dict {"k": k}
        cur_step.update(acc_dict)
        # Add current k and accuracies to the list
        acc_array.append(cur_step)

    # Plot the graph with accuracies
    k_array = [x.get('k') for x in acc_array]
    legend = []
    for A in algo_list:
        v_array = [x.get(A.__name__) for x in acc_array]
        plt.plot(k_array, v_array)
        legend.append(A.__name__)
    plt.legend(legend)
    plt.xlabel('$k$')
    plt.ylabel('$accuracy$')
    plt.grid(True)

    # Save graph or plot inline (or both)
    if plots_dir is not None:
        plt.savefig(f"{plots_dir}/r_{r}_n_{n}.png")
    if inline_plot:
        plt.show()

    # Return array with accuracies
    return acc_array


def full_simulation_waxman(algo_list,
                           n,
                           alpha,
                           beta,
                           k_start=1,
                           k_finish=2,
                           k_step=1,
                           n_trials=10,
                           output_file_path=None,
                           inline_plot=False,
                           plots_dir=None):
    """
    This function runs full simulation.
    For each k from interval [k_start, k_finish], it generates n_trials RGG(n, r) with a planted clique of size k.
    Then it runs each algorithm from algo_list, prints and returns obtained average accuracies.
    :param algo_list: array_like -- list of algo names to check during the simulation. Algorithms have to be classes.
    :param n: int -- Number of nodes in RGG
    :param alpha: double -- long/short edges ratio for Waxman graphs
    :param beta: double -- density parameter for Waxman graphs
    :param k_start: double -- Beginning of the interval [k_start, k_finish]
    :param k_finish: double -- Finish of the interval [k_start, k_finish]
    :param k_step: double -- Magnitude of a step when we go through the interval [k_start, k_finish]
    :param n_trials: int -- Number of generated graphs for each k
    :param output_file_path: str -- Path to the file where to save the results, optional
    :param inline_plot: bool -- Whether to show the plot inline
    :param plots_dir: str -- Path to folder where to save the plots
    :return: dict
    """
    # Create an array with possible values of accuracies
    acc_array = []

    # Clear the output file if it is set
    if output_file_path is not None:
        open(output_file_path, 'w').close()

    for k in np.arange(k_start, k_finish, k_step):
        cur_step = {"k": k}
        # Form the beginning of a string to print
        s = f"k = {k}, alpha = {alpha}, beta = {beta}, n = {n}, "

        # Form a dictionary with accuracies and times of execution for this k
        acc_dict = {A.__name__: 0 for A in algo_list}
        time_dict = {A.__name__: 0 for A in algo_list}

        # Generate graphs and check accuracy
        for i in range(n_trials):
            G = WaxmanPlantedClique(n, alpha, beta, k)

            # In a given trial, we use the same graph for all the algorithms
            for A in algo_list:
                # Launch algo A on graph G
                start_time = time.time()
                alg_res = A(G)
                time_sec = time.time() - start_time

                # Get accuracy and norm it by the number of trials in order to obtain average accuracy
                acc_dict[A.__name__] += alg_res.accuracy / n_trials
                time_dict[A.__name__] += time_sec

        # Form the string with results to print
        for A in algo_list:
            s = s + f"{A.__name__} = {np.around(acc_dict[A.__name__] * 100)}% ({int(time_dict[A.__name__])} sec), "
        print(s)

        # Save the same string to the file if needed
        if output_file_path is not None:
            with open(output_file_path, "a") as f:
                f.write(s)
                f.close()

        # Add all the accuracies to dict {"k": k}
        cur_step.update(acc_dict)
        # Add current k and accuracies to the list
        acc_array.append(cur_step)

    # Plot the graph with accuracies
    k_array = [x.get('k') for x in acc_array]
    legend = []
    for A in algo_list:
        v_array = [x.get(A.__name__) for x in acc_array]
        plt.plot(k_array, v_array)
        legend.append(A.__name__)
    plt.legend(legend)
    plt.xlabel('$k$')
    plt.ylabel('$accuracy$')
    plt.grid(True)

    # Save graph or plot inline (or both)
    if plots_dir is not None:
        plt.savefig(f"{plots_dir}/alpha_{alpha}_beta_{beta}_n_{n}.png")
    if inline_plot:
        plt.show()

    # Return array with accuracies
    return acc_array


def plot_adjacency_matrix(G):
    adj_matrix = nx.adjacency_matrix(G)
    adj_matrix = adj_matrix.toarray()

    n = G.number_of_nodes()
    clique_matrix = np.zeros((n, n))
    for (i, j) in itertools.combinations(G.planted_clique, 2):
        clique_matrix[i, j] = 1
        clique_matrix[j, i] = 1

    without_clique_matrix = adj_matrix - clique_matrix

    sns.set_style("whitegrid")
    plt.grid(b=None)
    markersize = 0.5
    plt.spy(adj_matrix, markersize=markersize)
    plt.show()

    sns.set_style("whitegrid")
    plt.grid(b=None)
    plt.spy(without_clique_matrix, markersize=markersize)
    # plt.spy(clique_matrix, markersize=markersize, color = 'red')
    plt.show()

    sns.set_style("whitegrid")
    plt.grid(b=None)
    plt.spy(without_clique_matrix, markersize=markersize)
    plt.spy(clique_matrix, markersize=markersize, color='red')
    # plt.spy(clique_matrix, markersize=markersize, color = 'red')
    plt.show()
